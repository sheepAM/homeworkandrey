import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.less']
})
export class Task1Component implements OnInit {

  content: string;
  showResult = false;

  constructor(public dataService: DataService) {
  }

  ngOnInit(): void {
    this.content =
      `<div #placeholder_first_name></div>

<div #placeholder_last_name></div>`;
  }

  /**
   * @method
   * @description Parsing typed text
   */
  parseTemplate() {
    this.dataService.items = [];

    const rawDataBlocks = this.content.match(/<div.*?>.*?<\/div>/g) || [];
    rawDataBlocks.forEach((block) => {
      if (/#placeholder_(\w|\d|)*(\s|>)/.test(block)) {
        const title = block.match(/(?<=#placeholder_)[\w|\d]*/)[0];
        this.dataService.items.push({
          title,
          value: null,
          rawData: block,
          result: ''
        });
      }
    });
  }

  /**
   * @method
   * @description Formation and display of the result
   */
  displayResult() {
    for (const item of this.dataService.items) {
      const startIndex = item.rawData.indexOf('>') + 1;
      const endIndex = item.rawData.indexOf('</div>', startIndex);
      const blockContent = `<ng-container> ${item.value || ''} </ng-container>`;

      item.result = `${item.rawData.substr(0, startIndex)} ${blockContent} ${item.rawData.substr(endIndex)}`;
    }
    this.showResult = true;
  }
}

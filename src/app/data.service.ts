import { Injectable } from '@angular/core';

export interface IItem {
  /**
   * @description Name for input field
   */
  title: string;
  /**
   * @description Input field value
   */
  value: string;
  /**
   * @description Raw data, user input
   */
  rawData: string;
  /**
   * @description Received Template
   */
  result: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  items: IItem[] = [];
}

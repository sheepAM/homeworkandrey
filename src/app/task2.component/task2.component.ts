import { Component } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  templateUrl: './task2.component.html',
  styleUrls: ['./task2.component.less']
})
export class Task2Component {
  constructor(public dataService: DataService) {
  }
}
